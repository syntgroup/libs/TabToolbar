/*
    TabToolbar - a small utility library for Qt, providing tabbed toolbars
	Copyright (C) 2018 Oleksii Sierov
	
    TabToolbar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TabToolbar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with TabToolbar.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef TT_STYLE_TOOLS_H
#define TT_STYLE_TOOLS_H
#include <QStringList>
#include <QStyle>
#include <functional>
#include <memory>
#include "tab-toolbar/tab-toolbar_export.hpp"
#include "tab-toolbar/Styles.h"

namespace tt
{
	
using TStyleCreator = std::function<StyleParams*()>;

TAB_TOOLBAR_EXPORT void                         RegisterStyle(const QString& styleName, const TStyleCreator& creator);
TAB_TOOLBAR_EXPORT void                         UnregisterStyle(const QString& styleName);
TAB_TOOLBAR_EXPORT bool                         IsStyleRegistered(const QString& styleName);
TAB_TOOLBAR_EXPORT QString                      GetSheetForStyle(const StyleParams& style);
TAB_TOOLBAR_EXPORT QStringList                  GetRegisteredStyles();
TAB_TOOLBAR_EXPORT std::unique_ptr<StyleParams> CreateStyle(const QString& styleName);
TAB_TOOLBAR_EXPORT QString                      GetDefaultStyle();

float                               GetScaleFactor(const QWidget& widget);
int                                 GetPixelMetric(QStyle::PixelMetric metric);

}
#endif
