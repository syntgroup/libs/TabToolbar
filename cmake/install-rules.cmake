if(PROJECT_IS_TOP_LEVEL)
  set(CMAKE_INSTALL_INCLUDEDIR include/tab-toolbar CACHE PATH "")
endif()

include(CMakePackageConfigHelpers)
include(GNUInstallDirs)

# find_package(<package>) call for consumers to find this project
set(package tab-toolbar)

install(
    DIRECTORY
    include/
    "${PROJECT_BINARY_DIR}/export/"
    DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}"
    COMPONENT tab-toolbar_Development
)

install(
    TARGETS tab-toolbar
    EXPORT tab-toolbarTargets
    RUNTIME #
    COMPONENT tab-toolbar_Runtime
    LIBRARY #
    COMPONENT tab-toolbar_Runtime
    NAMELINK_COMPONENT tab-toolbar_Development
    ARCHIVE #
    COMPONENT tab-toolbar_Development
    INCLUDES #
    DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}"
)

write_basic_package_version_file(
    "${package}ConfigVersion.cmake"
    COMPATIBILITY SameMajorVersion
)

# Allow package maintainers to freely override the path for the configs
set(
    tab-toolbar_INSTALL_CMAKEDIR "${CMAKE_INSTALL_LIBDIR}/cmake/${package}"
    CACHE PATH "CMake package config location relative to the install prefix"
)
mark_as_advanced(tab-toolbar_INSTALL_CMAKEDIR)

install(
    FILES cmake/install-config.cmake
    DESTINATION "${tab-toolbar_INSTALL_CMAKEDIR}"
    RENAME "${package}Config.cmake"
    COMPONENT tab-toolbar_Development
)

install(
    FILES "${PROJECT_BINARY_DIR}/${package}ConfigVersion.cmake"
    DESTINATION "${tab-toolbar_INSTALL_CMAKEDIR}"
    COMPONENT tab-toolbar_Development
)

install(
    EXPORT tab-toolbarTargets
    NAMESPACE tab-toolbar::
    DESTINATION "${tab-toolbar_INSTALL_CMAKEDIR}"
    COMPONENT tab-toolbar_Development
)

if(PROJECT_IS_TOP_LEVEL)
  include(CPack)
endif()
